const got = require('got');

class Twitter {
  constructor() {
    this.headers = {
      'Origin': 'https://mobile.twitter.com',
      'x-twitter-client-language': 'en',
      'x-csrf-token': '44352782d528bf9d879524118e0b5656',
      'authorization': 'Bearer AAAAAAAAAAAAAAAAAAAAANRILgAAAAAAnNwIzUejRCOuH5E6I8xnZz4puTs%3D1Zv7ttfk8LF81IUq16cHjhLTvJu4FA33AGWWjCpTnA',
      'content-type': 'application/x-www-form-urlencoded',
      'User-Agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1',
      'Referer': 'https://mobile.twitter.com/compose/tweet',
      'x-twitter-auth-type': 'OAuth2Session',
      'x-twitter-active-user': 'yes'
    };
    this.jar = {
      personalization_id: '"v1_O1LniygPDvlC/TuV8KAEdw=="',
      guest_id: 'v1%3A154882362517563252',
      ads_prefs: '"HBERAAA="',
      kdt: 'YpmdEMAEXIgflIJk3XJmaaGWJUoK6IkKXffBd0w2',
      remember_checked_on: 1,
      auth_token: 'd0ab7589b98c50e803845172bc1b755e098e9bf3',
      csrf_same_site_set: 1,
      csrf_same_site: 1,
      eu_cn: 1,
      _ga: 'GA1.2.2076573421.1550720199',
      tfw_exp: 0,
      _gid: 'GA1.2.1905805895.1562121156',
      twtr_pixel_opt_in: 'Y',
      mbox: 'check#true#1562121712|session#a867ea0d8d1b4d88808c2f80918ba4ad#1562123512|PC#a867ea0d8d1b4d88808c2f80918ba4ad.26_18#1563331252',
      external_referer: 'padhuUp37zjgzgv1mFWxJ12Ozwit7owX|0|8e8t2xd8A2w%3D',
      _twitter_sess: 'BAh7CSIKZmxhc2hJQzonQWN0aW9uQ29udHJvbGxlcjo6Rmxhc2g6OkZsYXNo%250ASGFzaHsABjoKQHVzZWR7ADoPY3JlYXRlZF9hdGwrCKlP%252FL9rAToHaWQiJTYw%250ANTFlNWE1YmQzNjJjZmM4ZjhlNjg5ZGRmMzgzMTg2Ogxjc3JmX2lkIiVmYTUx%250AOWJhMTI2YmI5MWNiMTE5YjkzODk1YjQxODc1Zg%253D%253D--45b8e218b5616beddfb81d9ae7952551bc3dc818',
      ct0: '44352782d528bf9d879524118e0b5656',
      twid: 'u%3D445381090',
      _gat: 1,
    }
  }
  async tweet(status) {
    try {
      const cookie = this.getCookie();
      const response = await got('https://api.twitter.com/1.1/statuses/update.json', {
        method: 'POST',
        headers: {
          ...this.headers,
          cookie
        },
        form: {
          include_profile_interstitial_type: 1,
          include_blocking: 1,
          include_blocked_by: 1,
          include_followed_by: 1,
          include_want_retweets: 1,
          include_mute_edge: 1,
          include_can_dm: 1,
          include_can_media_tag: 1,
          skip_status: 1,
          cards_platform: 'Web-12',
          include_cards: 1,
          include_composer_source: true,
          include_ext_alt_text: true,
          include_reply_count: 1,
          tweet_mode: 'extended',
          trim_user: false,
          include_ext_media_color: true,
          include_ext_media_availability: true,
          auto_populate_reply_metadata: false,
          batch_mode: 'off',
          status
        }
      });
      this.setCookie(response.headers['set-cookie']);
      return response.body;
    } catch (e) {
      let message;
      try {
        message = e.body;
      } catch(e) {}
      return message || e.message;
    }
  }
  getCookie() {
    let array = [];
    for (let key in this.jar) {
      array.push(`${key}=${this.jar[key]}`)
    }
    return array.join(';');
  }
  setCookie(cookies) {
    if (!Array.isArray(cookies)) return;
    for (let cookie of cookies) {
      cookie = ( cookie.split(';')[0] ).split('=');
      let key = cookie[0], value = cookie[1];
      if (value) {
        this.jar[key] = value;
      }
    }
    return this.jar;
  }
}

module.exports = Twitter