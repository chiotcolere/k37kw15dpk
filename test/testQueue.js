const queue = require('../modules/getQueue');

(async () => {
  console.log(queue.add(async () => {
    await new Promise((resolve, reject) => setTimeout(reject, 1000));
    console.log(1);
  }).catch(console.log.bind(null, 'a')));
  queue.add(async () => {
    await new Promise(resolve => setTimeout(resolve, 1000));
    throw new Error('3');
    // console.log(2);
  }).catch(console.log.bind(null, 'b'));
})();
