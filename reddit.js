const qs = require('querystring');
const got = require('got');

class Reddit {
  constructor() {
    this.config = {
      ua: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:71.0) Gecko/20100101 Firefox/71.0',
      referer: 'https://www.reddit.com/',
      origin: 'https://www.reddit.com',
      accessToken: '17943558-W7jfqieoE0XNoeKTnKc7Z2-s3ls'
    };
    this.jar = {
      loid: '0000000000000aolc6.2.1361467526450.Z0FBQUFBQmVDRXBNdXBPQ0tuNmlFTkFJQjBvZlQyV0N5emtfNEFReXg2b3VmV1ZfazJUbktRU0dwd3lqckRlcFJ1VlZqaUJETHgyeUtMRjh2NGt5VFFlTlU5WE9nVmVNek9aUU53VkdrUHZGNjZNbE9DWTJtRFA2MkxyNk5MLTA3aGhxWm5WcjZQQ18',
      edgebucket: 'U7cmWo6vuvBZ5VevcB',
      reddaid: 'VKZLPJ5KXH3L66YA',
      reddit_session: '17943558%2C2019-12-29T06%3A39%3A59%2C547769709ab71fc3a860beff64eddc9074943144',
      USER: 'eyJwcmVmcyI6eyJnbG9iYWxUaGVtZSI6IlJFRERJVCIsImZlYXR1cmVzVmlld2VkSGlzdG9yeSI6eyJjb21tZW50Rm9ybSI6eyJtYXJrZG93bk1vZGVOb3RpZmljYXRpb24iOnRydWV9fSwiY29sbGFwc2VkVHJheVNlY3Rpb25zIjp7ImZhdm9yaXRlcyI6ZmFsc2UsIm11bHRpcyI6ZmFsc2UsIm1vZGVyYXRpbmciOmZhbHNlLCJzdWJzY3JpcHRpb25zIjpmYWxzZSwicHJvZmlsZXMiOmZhbHNlfSwibmlnaHRtb2RlIjpmYWxzZSwicnBhbkR1RGlzbWlzc2FsVGltZSI6bnVsbCwidG9wQ29udGVudERpc21pc3NhbFRpbWUiOm51bGwsInRvcENvbnRlbnRUaW1lc0Rpc21pc3NlZCI6MH0sImxhbmd1YWdlIjoiZW4ifQ==',
      recent_srs: 't5_vf2%2Ct5_2t9zr%2Ct5_300m6%2Ct5_2qq5c%2Ct5_37n3u%2Ct5_3bkmw%2Ct5_3acrs%2Ct5_38ob7%2Ct5_2sbq3%2Ct5_wqz2d',
      eu_cookie_v2: 3,
      d2_token: '3.bc8f9a4856b2a9ef014657b92eab53eadf208aa7aafb6ecc3ff38d34a278b2e5.eyJhY2Nlc3NUb2tlbiI6IjE3OTQzNTU4LW5lbkljYWR2aDhCM2NLaVJmNGF1bFJmSDJzbyIsImV4cGlyZXMiOiIyMDIwLTAxLTEwVDE4OjAwOjQ2LjAwMFoiLCJsb2dnZWRPdXQiOmZhbHNlLCJzY29wZXMiOlsiKiIsImVtYWlsIl19',
      __gads: 'ID=ad2ac7c29c8500b0:T=1573998806:S=ALNI_MbFQplnvE0jKhMtWf9Ps4Mltv9obA',
      pc: '2y',
      session_tracker: '4vWizSN7hU9dtCgY53.0.1578675654728.Z0FBQUFBQmVHSzNHd1VfSWNucXNWVVBGWm4xY0xXUTU1OWhXbUJaT1BEejNtWl9UbFBGTFlXNFA2UXltYXh1UHRYQnRXOVBXcFRlSVlXaWxsQlNLQWxvaHlvR0owdlIyYksyOHdHdGhOR1paeHU4Y3RLM0NrYk05T0dVT2QzWDdnUFpSaHA0dlJ1Nkk',
      aasd: '1%7C1578675656536',
      __aaxsc: 0
    }
    // this.init();
  }
  async init() {
    const cookie = this.getCookie();
    const { ua, referer, origin } = this.config;
    try {
      const response = await got('https://www.reddit.com', {
        headers: {
          origin,
          referer,
          cookie,
          'user-agent': ua
        }
      });
      // console.log({response});
      this.setCookie(response.headers['set-cookie']);
      this.config = {
        ...this.config,
        ...this.accessTokenRipper(response.body)
      };
      // console.log('session_tracker', this.jar.session_tracker);
      // console.log('accessToken', this.config.accessToken);
    } catch(e) {
      console.warn({api: 'init', e});
    }
  }
  async fetchTitle(url) {
    try {
      const {
        loid,
        session_tracker
      } = this.jar;
      const {
        accessToken,
        expires,
        referer,
        origin,
        ua
      } = this.config;
      if (!(new Date() < expires)) {
        await this.init();
        return this.fetchTitle(url);
      }
      const response = await got('https://oauth.reddit.com/api/fetch_title?redditWebClient=web2x&app=web2x-client-production&raw_json=1&gilding_detail=1', {
        method: 'POST',
        headers: {
          origin,
          referer,
          'user-agent': ua,
          'x-reddit-loid': loid,
          'x-reddit-session': session_tracker,
          Authorization: `Bearer ${accessToken}`
        },
        form: {
          url,
          api_type: 'json'
        }
      });
      const { json } = JSON.parse(response.body);
      return json.data.title;
    } catch(e) {
      console.warn({api: 'fetchTitle', e});
      return '';
    }
  }
  async postLink({
    sr,
    title,
    url,
    name
  } = {}) {
    const {
      loid,
      session_tracker
    } = this.jar;
    const {
      accessToken,
      expires,
      referer,
      origin,
      ua,
    } = this.config;
    if (!(new Date() < expires)) {
      await this.init();
      return this.postLink({ sr, title, url });
    }
    if (!title && url) {
      title = await this.fetchTitle(url);
    }
    try {
      const body = {
        api_type: 'json',
        sr,
        submit_type: 'subreddit',
        title,
        spoiler: false,
        nsfw: false,
        original_content: false,
        post_category: '',
        post_to_twitter: false,
        kind: 'link',
        sendreplies: true,
        url,
        validate_on_submit: true
      };
      if (name) {
        body.kind = 'crosspost';
        body.crosspost_fullname = name;
      } else {
        body.kind = 'link';
        body.url = url;
      }
      const response = await got('https://oauth.reddit.com/api/submit?resubmit=true&redditWebClient=web2x&app=web2x-client-production&rtj=only&raw_json=1&gilding_detail=1', {
        method: 'POST',
        headers: {
          origin,
          referer,
          'user-agent': ua,
          'x-reddit-loid': loid,
          'x-reddit-session': session_tracker,
          Authorization: `Bearer ${accessToken}`
        },
        form: body
      });
      return response.body;
    } catch (e) {
      if (e.statusCode === 401) {
        this.init();
      }
      let message;
      try {
        message = e.body;
      } catch(e) {}
      return message || e.message;
    }
  }
  async postLinkMobile({
    sr,
    title,
    url,
    name
  } = {}) {
    const {
      accessToken,
      expires,
      referer,
      origin,
      ua
    } = this.config;
    if (!(new Date() < expires)) {
      await this.init();
      return this.postLinkMobile({ sr, title, url });
    }
    if (!title) {
      title = await this.fetchTitle(url);
    }
    try {
      const body = {
        redditWebClient: 'mweb2x',
        layout: 'card',
        api_type: 'json',
        title,
        // kind: 'link',
        sendreplies: true,
        sr,
        resubmit: false,
        // url,
        obey_over18: true
      };
      if (name) {
        body.kind = 'crosspost';
        body.crosspost_fullname = name;
      } else {
        body.kind = 'link';
        body.url = url;
      }
      const api = `https://oauth.reddit.com/api/submit?${qs.stringify(body)}`;
      // console.log({api});
      const response = await got(api, {
        method: 'POST',
        headers: {
          origin,
          referer,
          'user-agent': ua,
          Authorization: `Bearer ${accessToken}`
        },
        body: '{}'
      });
      return response.body;
    } catch (e) {
      if (e.statusCode === 401) {
        this.init();
      }
      return e.body || e.message;
    }
  }
  async getGQL() {
    const {
      loid,
      session_tracker
    } = this.jar;
    const {
      accessToken,
      expires,
      referer,
      origin,
      ua,
    } = this.config;
    if (!(new Date() < expires)) {
      await this.init();
      return this.getGQL();
    }
    try {
      const response = await got(`https://gql.reddit.com/?request_timestamp=${Date.now()}`, {
        method: 'POST',
        headers: {
          origin,
          referer,
          authorization: `Bearer ${accessToken}`,
          'user-agent': ua,
          'x-reddit-loid': loid,
          'x-reddit-session': session_tracker,
          'content-type': 'application/json'
        },
        body: '{"id":"d2b39ab0293a","variables":{"subredditName":"chn"}}'
      });
      return response.body;
    } catch (e) {
      return e.message;
    }
  }
  async getSub({sr} = {}) {
    const {
      accessToken,
      expires,
      referer,
      origin,
      ua
    } = this.config;
    if (!(new Date() < expires)) {
      await this.init();
      return this.getSub({sr});
    }
    try {
      const api = `https://oauth.reddit.com/r/${sr}.json?${qs.stringify({
        redditWebClient: 'mweb2x',
        layout: 'card',
        raw_json: 1,
        withAds: true,
        subredditName: sr,
        feature: 'link_preview',
        sr_detail: true,
        allow_over18: 1,
        app: '2x-client-production',
        obey_over18: true
      })}`;
      // console.log({api});
      const response = await got(api, {
        headers: {
          origin,
          referer,
          'user-agent': ua,
          Authorization: `Bearer ${accessToken}`
        }
      });
      return response.body;
    } catch (e) {
      if (e.statusCode === 401) {
        this.init();
      }
      let message;
      try {
        message = e.body;
      } catch(e) {}
      return message || e.message;
    }
  }
  accessTokenRipper(body){
    let mark = 'accessToken":"';
    let index = body.indexOf(mark);
    if (index === -1) {
      return '';
    }
    body = body.slice(index + mark.length);
    const accessToken = body.slice(0, body.indexOf('"'));
    mark = 'expires":"';
    index = body.indexOf(mark);
    body = body.slice(index + mark.length);
    const expires = new Date(body.slice(0, body.indexOf('"')));
    return {
      accessToken,
      expires
    };
  }
  getCookie() {
    let array = [];
    for (let key in this.jar) {
      array.push(`${key}=${this.jar[key]}`)
    }
    return array.join(';');
  }
  setCookie(cookies) {
    if (!Array.isArray(cookies)) return;
    for (let cookie of cookies) {
      cookie = ( cookie.split(';')[0] ).split('=');
      let key = cookie[0], value = cookie[1];
      if (value) {
        this.jar[key] = value;
      }
    }
    return this.jar;
  }
}

module.exports = Reddit