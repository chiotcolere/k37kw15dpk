const got = require('got');

class Pinterest {
  constructor() {
    this.headers = {
      'content-type': 'application/x-www-form-urlencoded',
      'accept': 'application/json, text/javascript, */*, q=0.01',
      'authority': 'www.pinterest.com',
      'cache-control': 'no-cache',
      'origin': 'https://www.pinterest.com',
      'referer': 'https://www.pinterest.com/',
      'user-agent': 'Mozilla/5.0 (iPhone; CPU iPhone OS 11_0 like Mac OS X) AppleWebKit/604.1.38 (KHTML, like Gecko) Version/11.0 Mobile/15A372 Safari/604.1',
      'x-app-version': '1e82afe',
      'x-csrftoken': '170e2097fa504d72873ba90683dd9e45',
      'x-pinterest-appstate': 'active',
      'x-pinterest-experimenthash': '96dc97e3fa3869407de8b6cc080f4db472950f367cb135b00b4695b778169636124c5ec1cdf1050f8418cff429456b603cf2590e3a4567113e2cbda00848d9e5',
      'x-pinterest-mweb': '1',
      'x-requested-with': 'XMLHttpRequest'
    };
    this.jar = {
      csrftoken: '170e2097fa504d72873ba90683dd9e45',
      _auth: 1,
      _pinterest_sess: 'TWc9PSZrcEZ2b1krUk96aEdqN2tqUkZVN0t4N01YeWd3S0dyYkI0L3ZVNmJhWFZneGZwYm00alE1VmFzWlRVTG5ublZDTkVsQ0czZWVaOGVLQkdoSnpBSGlCYjAzTDlQZVdNYWdONXBDcHNFUnh6OXk2TVlvanE5c3BtVHRFVSsyWmZ2RTRmcmUvalhGR3dHYkllMnNnWVE2ekR6MkQwYUxmN3I1RzJKVFBYWWVLSDhyZ2I5bCtBLzhMbFg4OWdMckZrRFI2a29ZV3UrZVdmQTRSWWlEUVFDcmpIRnZsSmJ3WEpKVHdpMkVBMUhGcGJSVGVyYlZiQ3JySnQxWDBxQzA0T0hITGVIYXNZN0JsTUEzcjdoVnJZT0ZHbGtjSEFoOVNBSjdQZVpsVjFkRkZhK0RRZUNnNHRuVTVkdXdWUDl3OU5xWTVNU2M2Y2tjL0FoWUVxWjdLbHlPRnN1ZnUxUWRjVmV2ZGJMS0k1bE5ZQ1U1YW0zTGcxMlpOa2xGRmNJUWUxODBDR0N0OEZaa0wyT1BtSEtZcUhPTXhYTno1dytqeDZCOGsvMFdEMThJbXVJSnJOVEZsYUZWdm84SDhZd3ZCa0dZbFJXMU5LNnRFUk1ySld2Z0d4aDdNMVJ2MDJJbllzeVhBNk5GOUZXYkdsZlFJN3hreDY2S24xb1RLd05nYTVRTGdsVTc4TUNkWk9uVjJPOWlKYjJkZmZaUGhWZ2VEQytXYWkrclFHalVDSHNGVjdWN3VDcFVIVUIvTFdaYmpDN1E2b2xhdFlJWWJwWFJGOVM4eUg0VDNQdWx3UysxSnhaNDExd2ZLS0p4QkRueTNXVWFacUhpS3BRbDVJUS95blYwNzVoVkFDdHJ3NWFsRHVuSG1QUEx0bzdwOUhxTFZ1eVVlR1YxaTNYeUpCMlpyVExyTWVNQWx3V2daTG1HN043WktudmNPc0t1L2laTmwyQjdCVGYzK0Y5VFNvWnZzaDlPWnBGQU4rdzI0ZDBJN3h0cFFmZi9PZHkyUGh6STR5MWY3eG9jQnppbHVLL3NyeEsvTTN5dnAwTy85dXN5Z3BJamY4Q3pCUlpzRWk3M2lNTVRacFVBcSt0MDhsVUkwMWk4Jk1ON05nOVZDMXVBUHRuWHZuL1ViSzZWYzFBQT0=',
      cm_sub: 'denied',
      _b: '"AUsnifHi+b9HPJ1mxLCPU52uEsVA4cBAESkhYkoN2EAv8nkJes+KW/YNkL4M6YC+vJ8="',
      _routing_id: '"bd9499e8-4d46-4547-aef8-8b7e91d08b56"',
      sessionFunnelEventLogged: 1,
      bei: false,
      _pinterest_pfob: 'enabled'
    };
  }
  async init() {
    try {
      const cookie = this.getCookie();
      const response = await got('https://www.pinterest.com', {
        headers: {
          ...this.headers,
          cookie
        }
      });
      this.setCookie(response.headers['set-cookie']);
    } catch (e) {
      console.warn({api: 'init', e});
    }
  }
  async pin({
    board_id,
    description = '',
    image_url,
    link
  } = {}) {
    if (!board_id) {
      board_id = '204139864298707322';
    }
    if (!link) {
      link = image_url;
    }
    try {
      const cookie = this.getCookie();
      const data = JSON.stringify({
        options: {
          board_id,
          description,
          image_url,
          link,
          method: 'pinbuilder'
        },
        context: {}
      });
      const response = await got('https://www.pinterest.com/resource/PinResource/create/', {
        method: 'POST',
        headers: {
          ...this.headers,
          cookie
        },
        form: {
          source_url: '/pin/create/pinbuilder/',
          data
        }
      });
      this.setCookie(response.headers['set-cookie']);
      return response.body;
    } catch (e) {
      this.init();
      let message;
      try {
        message = e.body;
      } catch(e) {}
      return message || e.message;
    }
  }
  getCookie() {
    let array = [];
    for (let key in this.jar) {
      array.push(`${key}=${this.jar[key]}`)
    }
    return array.join(';');
  }
  setCookie(cookies) {
    if (!Array.isArray(cookies)) return;
    for (let cookie of cookies) {
      cookie = ( cookie.split(';')[0] ).split('=');
      let key = cookie[0], value = cookie[1];
      if (value) {
        this.jar[key] = value;
      }
    }
    return this.jar;
  }
}

module.exports = Pinterest