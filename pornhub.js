// const fs = require('fs');
// const path = require('path');
const request = require('request-promise-native');
// const tmpFilePath = path.join(__dirname, './tmp.js');
const sendToRobot = require('./modules/sendToRobot');
const processPornhubHtml = require('./modules/processPornhubHtml');

const pornhub = async (url, cookie) => {
  // console.log({url});
  const id = url.split('viewkey=')[1].split('?')[0];
  const headers = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9',
    'Accept-Language': 'en-US,en;q=0.9,zh;q=0.8,zh-CN;q=0.7,zh-TW;q=0.6,ja;q=0.5,ko;q=0.4,ru;q=0.3',
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.34 Safari/537.36',
    'Referer': url
  }
  if (cookie) {
    headers.Cookie = cookie
  }
  let html = await request({
    url,
    headers,
    gzip: true
  });
  // let html = require('fs').readFileSync('./test.html');
  // require('fs').writeFileSync('./test.html', html);
  const {
    title,
    // context,
    result,
  } = processPornhubHtml(html);
  // require('fs').writeFileSync('./context.json', JSON.stringify(context, 2, null));
  sendToRobot(
    JSON.stringify({
      title,
      result,
    }, null, 2),
  );
  return {
    id,
    title,
    // ...context,
    ...result,
  };
}

module.exports = pornhub;