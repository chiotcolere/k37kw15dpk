const cluster = require('cluster');
const os = require('os');
const { name, version } = require('./package.json');
const sendToRobot = require('./modules/sendToRobot');

// process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';

if (cluster.isMaster) {
  sendToRobot(`[${os.hostname()} ${name}] ${version} started`);
  cluster.fork();
  // eslint-disable-next-line no-unused-vars
  cluster.on('exit', (worker, code, signal) => {
    sendToRobot(`[${os.hostname()} ${name}] ${version} restarted:\nworker:${worker}\ncode:${code}\nsignal:${signal}`);
    cluster.fork();
  });
}
if (cluster.isWorker) {
  const { inspect } = require('util');
  global.process.on('uncaughtException', e => {
    sendToRobot(`[${os.hostname()} ${name}] ${version} has an uncaught exception:\ninspect:\n${inspect(e, { showHidden: true })}`);
    // global.process.exit(1);
  });
  const http = require('http');
  const fetch = require('request');
  // const lzjb = require('lzjb');
  // const lz4 = require('lz4');
  const wsServer = require('./modules/wsServer');
  const uploadToMEGA = require('./modules/uploadToMEGA');
  const youtubeDL = require('./modules/youtubeDL');
  const youtube = require('ytdl-core');
  const pornhub = require('./pornhub.js');
  const util = require('util');
  const lookup = util.promisify(require('dns').lookup);
  const allowCORS = require('./modules/allowCORS');
  const findVideosToUpload = require('./modules/findVideosToUpload');

  // const qs = require('querystring');
  let Instagram = require('./instagram');
  let Reddit = require('./reddit');
  let Pinterest = require('./pinterest');
  let Twitter = require('./twitter');
  Twitter = new Twitter();
  Pinterest = new Pinterest();
  Reddit = new Reddit();
  // Reddit.init();
  // setInterval(console.log.bind(null, new Date()), 60000);
  Instagram = new Instagram();
  // Instagram.userWeb('aerowindwalker').then(res => console.log(JSON.parse(res).entry_data.ProfilePage[0].graphql.user));
  const parse = (request, response) => {
    return new Promise(resolve => {
      let requestBody = '';
      request.on('data', data => {
        requestBody += data;
        if(requestBody.length > 2048) {
          response.writeHead(413, 'Request Body Too Large', {'Content-Type': 'text/html'});
          response.end('413: Request Body Too Large');
          // reject(413);
          resolve(requestBody);
        }
      });
      request.on('end', () => resolve(requestBody));
      request.on('error', () => resolve(requestBody));
    });
  }
  const pictureToken = 'yd4mIxd%I5';
  // eslint-disable-next-line complexity
  const app = async (request, response) => {
    function failed(message) {
      // response.writeHead(500, {'Content-Type': 'text/html'});
      allowCORS(200, response);
      response.end(`[failed]${'\n'}${message}`);
    }
    function redirect(){
      response.writeHeader(302, {
        'Location': 'https://buy.stripe.com/eVa14j7gF9cI0Hm6oo',
        'Node.js': process.version || 'N/A',
        'Version': version,
      });
      response.end();
    }
    // function notfound(){
    //   response.writeHead(404);
    //   response.end('404');
    // }
    if (request.method === 'GET' && request.url.indexOf('/p/') === 0) {
      // const { hash } = qs.parse(request.url);
      const hash = request.url.slice(3);
      if (!hash) {
        return redirect();
      }
      try {
        const target = Buffer.from(decodeURIComponent(hash), 'base64').toString().replace(pictureToken, 'http');
        if (target.indexOf('http') !== 0) {
          return redirect();
        }
        return fetch(target).on('response', res => {
          response.writeHead(res.statusCode, res.headers);
        }).on('error', e => {
          if (/^getaddrinfo ENOTFOUND/.test(e.message)) {
            response.writeHead(401);
            response.end('forbidden');
          } else {
            response.writeHead(500);
            response.end(e.message);
          }
        }).pipe(response);
      } catch (e) {
        return failed(e.message);
      }
    }
    if (request.url.indexOf('/v/') === 0) {
      try {
        const str = request.url.slice(3);
        if (!str)
          return redirect();
        // console.log(request.headers);
        let rawRange = request.headers.range;
        let range;
        if (rawRange) {
          range = rawRange.split('=')[1].split('-');
          range = JSON.parse(JSON.stringify({
            start: range[0],
            end: range[1]
          }));
        }
        // console.log({range});
        const stream = youtube(`http://www.youtube.com/watch?v=${str}`, range ? { range } : {});
        let {
          name,
          contentLength
        } = await new Promise(resolve => {
          stream
            .on('info', (info, format) => {
              const { title } = info.videoDetails;
              // console.log(JSON.stringify(info, null, 2));
              // console.log(info.formats);
              let contentLength;
              try {
                contentLength = info.formats[0].clen;
              } catch(e) {}
              resolve({
                name: encodeURIComponent(`${title.replace(new RegExp('\'', 'gu'), '')}_${str}.mp4`),
                contentLength
              });
            })
            .on('error', e => {
              resolve({});
              failed(e.message);
            });
        });
        if (!name) {
          return failed('no name!');
        }
        if (!contentLength) {
          contentLength = await new Promise(resolve => {
            stream.on('progress', (_chunk, _downloaded, contentLength) => {
              resolve(contentLength);
            });
          });
        }
        // console.log({name, contentLength});
        if (range) {
          // console.log(`${rawRange.split('=').join(' ')}/${parseInt(range.end) + 1}`);
          response.writeHead(206, {
            // 'Accept-Ranges': 'bytes',
            'Content-Range': `${rawRange.split('=').join(' ')}/${parseInt(range.end, 10) + 1}`,
            'Content-Length': contentLength
          });
        } else {
          response.writeHead(200, {
            'Accept-Ranges': 'bytes',
            'Content-Disposition': `attachment; filename*=UTF-8''${name}`,
            'Content-Length': contentLength
          });
        }
        return stream.pipe(response);
      } catch(e) {
        return failed(e.message);
      }
    }
    // if (request.url.indexOf('/h/') === 0) {
    //   try {
    //     const str = request.url.slice(3);
    //     if (!str)
    //       return redirect();
    //     const result = await pornhub(str);
    //     response.writeHead(200);
    //     response.end(result);
    //   } catch(e) {
    //     return notfound();
    //   }
    // }
    if (request.method !== 'POST')
      return redirect();
    if (request.url === '/') {
      let data = await parse(request, response) || '{}';
      try {
        data = JSON.parse(data);
      } catch(e) {
        return failed('A001');
      }
      let {
        query_hash,
        end_cursor,
        csrftoken,
        sessionid,
        // rhx_gis
        www_claim
      } = data;
      if (!csrftoken || !sessionid || !www_claim || !query_hash) {
        return failed('A002');
      }
      Instagram.config({
        csrftoken,
        sessionid,
        www_claim
      });
      try {
        let t = await Instagram.getFeed(50, end_cursor, query_hash);
        // t = JSON.parse(t);
        // let feed = t.data.user.edge_web_feed_timeline;
        response.writeHead(200);
        response.end(t);
      } catch(e) {
        return failed('A003: ' + e.message);
      }
    }
    if (request.url === '/p') {
      let data = await parse(request, response) || '{}';
      try {
        data = JSON.parse(data);
      } catch(e) {
        return failed('A001');
      }
      let {
        target,
        token
      } = data;
      if (!target || !token) {
        return failed('A002');
      }
      if (token !== pictureToken) {
        return failed('A003');
      }
      try {
        return fetch(target).on('response', res => {
          response.writeHead(res.statusCode, res.headers);
        }).pipe(response);
      } catch(e) {
        return failed(e.message);
      }
    }
    if (request.url === '/f') {
      let data = await parse(request, response) || '{}';
      try {
        data = JSON.parse(data);
      } catch(e) {
        return failed('A001');
      }
      let {
        target,
        token,
        cookie,
        filename,
      } = data;
      if (!target || !token) {
        return failed('A002');
      }
      if (token !== pictureToken) {
        return failed('A003');
      }
      try {
        const options = {
          url: target,
          cookie,
          filename: filename || target,
        };
        allowCORS(200, response);
        response.end(JSON.stringify(options));
        return uploadToMEGA(options);
      } catch (e) {
        return failed(e.message);
      }
    }
    if (request.url === '/dl') {
      let data = await parse(request, response) || '{}';
      try {
        data = JSON.parse(data);
      } catch(e) {
        return failed('A001');
      }
      let {
        target,
        token,
        flags,
      } = data;
      if (!target || !token) {
        return failed('A002');
      }
      if (token !== pictureToken) {
        return failed('A003');
      }
      if (flags && !Array.isArray(flags)) {
        return failed('A004');
      }
      return youtubeDL(target, response, failed, flags);
    }
    if (request.url === '/findVideosToUpload') {
      let data = await parse(request, response) || '{}';
      try {
        data = JSON.parse(data);
      } catch(e) {
        return failed('A001');
      }
      const { token } = data;
      if (!token) {
        return failed('A002');
      }
      if (token !== pictureToken) {
        return failed('A003');
      }
      findVideosToUpload();
      response.writeHead(200);
      response.end('ok');
    }
    if (request.url === '/h') {
      let data = await parse(request, response) || '{}';
      try {
        data = JSON.parse(data);
      } catch(e) {
        return failed('A001');
      }
      let {
        target,
        token,
        cookie
      } = data;
      if (!target || !token) {
        return failed('A002');
      }
      if (token !== pictureToken) {
        return failed('A003');
      }
      try {
        // console.log({ target });
        const {
          id,
          title,
          quality_240p,
          quality_480p,
          quality_720p,
          quality_1080p,
        } = await pornhub(target, cookie);
        if (!quality_1080p && !quality_720p && !quality_480p && !quality_240p) {
          return failed('no media found');
        }
        const options = {
          url:
            quality_1080p ||
            quality_720p ||
            quality_480p ||
            quality_240p,
          // url: quality_480p || quality_240p,
          cookie,
          filename: `[pornhub]${title}_${id}.mp4`,
        };
        allowCORS(200, response);
        response.end(JSON.stringify(options));
        return uploadToMEGA(options);
      } catch (e) {
        return failed(e.message);
      }
    }
    if (request.url === '/v') {
      let data = await parse(request, response) || '{}';
      try {
        data = JSON.parse(data);
      } catch(e) {
        return failed('A001');
      }
      let {
        target,
        token
      } = data;
      if (!target || !token) {
        return failed('A002');
      }
      if (token !== pictureToken) {
        return failed('A003');
      }
      try {
        // const info = await youtube.getInfo(
        //   target.startsWith('http') ? target : `http://www.youtube.com/watch?v=${target}`
        // );
        const info = await youtube.getInfo(target);
        // return failed(JSON.stringify(info, null, 2));
        const {
          formats,
          // adaptiveFormats
        } = info.player_response.streamingData;
        const { title } = info.videoDetails || {};
        // const results = [
        //   ...formats,
        //   ...adaptiveFormats
        // ].filter(o => o.contentLength);
        // return response.end(JSON.stringify(results));
        // const result = results.filter(o => o.width === 1280)[0] || results[0];
        const result = formats[1] || formats[0];
        const {
          contentLength: filesize,
          url
        } = result;
        const options = {
          url,
          filename: `[youtube]${title}_${target}.mp4`,
          filesize,
        };
        allowCORS(200, response);
        response.end(JSON.stringify(options));
        return uploadToMEGA(options);
      } catch(e) {
        return failed(e.message);
      }
    }
    if (request.url === '/user') {
      let data = await parse(request, response) || '{}';
      try {
        data = JSON.parse(data);
      } catch(e) {
        return failed('A001');
      }
      let {
        csrftoken,
        sessionid,
        // rhx_gis,
        www_claim,
        username
      } = data;
      if (!csrftoken || !sessionid || !www_claim || !username) {
        return failed('A002');
      }
      Instagram.config({
        csrftoken,
        sessionid,
        www_claim
      });
      try {
        let t = await Instagram.user(username);
        response.writeHead(200);
        response.end(t);
      } catch(e) {
        return failed('A003: ' + e.message);
      }
    }
    if (request.url === '/userweb') {
      let data = await parse(request, response) || '{}';
      try {
        data = JSON.parse(data);
      } catch(e) {
        return failed('A001');
      }
      let {
        csrftoken,
        sessionid,
        username
      } = data;
      if (!csrftoken || !sessionid || !username) {
        return failed('A002');
      }
      Instagram.config({
        csrftoken,
        sessionid
      });
      try {
        let t = await Instagram.userWeb(username);
        response.writeHead(200);
        response.end(t);
      } catch(e) {
        return failed('A003: ' + e.message);
      }
    }
    if (request.url === '/userfeed') {
      let data = await parse(request, response) || '{}';
      try {
        data = JSON.parse(data);
      } catch(e) {
        return failed('A001');
      }
      let {
        query_hash,
        csrftoken,
        sessionid,
        // rhx_gis,
        www_claim,
        after,
        id
      } = data;
      if (!query_hash || !csrftoken || !sessionid || !www_claim || !id) {
        return failed('A002');
      }
      Instagram.config({
        csrftoken,
        sessionid,
        www_claim
      });
      try {
        let t = await Instagram.getUserFeed({
          id,
          first: 32,
          after,
          query_hash
        });
        response.writeHead(200);
        response.end(t);
      } catch(e) {
        return failed('A003: ' + e.message);
      }
    }
    if (request.url === '/like' || request.url === '/unlike') {
      let data = await parse(request, response) || '{}';
      try {
        data = JSON.parse(data);
      } catch(e) {
        return failed('A001');
      }
      let {
        csrftoken,
        sessionid,
        rollout_hash,
        id
      } = data;
      if (!csrftoken || !sessionid || !id) {
        return failed('A002');
      }
      Instagram.config({
        csrftoken,
        sessionid,
        rollout_hash
      });
      try {
        let t = await Instagram.like(request.url, id);
        response.writeHead(200);
        response.end(t);
      } catch(e) {
        return failed('A003: ' + e.message);
      }
    }
    if (request.url === '/follow' || request.url === '/unfollow') {
      let data = await parse(request, response) || '{}';
      try {
        data = JSON.parse(data);
      } catch(e) {
        return failed('A001');
      }
      let {
        csrftoken,
        sessionid,
        rollout_hash,
        id
      } = data;
      if (!csrftoken || !sessionid || !id) {
        return failed('A002');
      }
      Instagram.config({
        csrftoken,
        sessionid,
        rollout_hash
      });
      try {
        let t = await Instagram.follow(request.url, id);
        response.writeHead(200);
        response.end(t);
      } catch(e) {
        return failed('A003: ' + e.message);
      }
    }
    if (request.url === '/postlink') {
      let data = await parse(request, response) || '{}';
      try {
        data = JSON.parse(data);
      } catch(e) {
        return failed('A001');
      }
      // console.log({data});
      let text = await Reddit.postLink(data);
      response.writeHead(200);
      response.end(text);
    }
    if (request.url === '/postlinkmobile') {
      let data = await parse(request, response) || '{}';
      try {
        data = JSON.parse(data);
      } catch(e) {
        return failed('A001');
      }
      // console.log({data});
      let text = await Reddit.postLinkMobile(data);
      response.writeHead(200);
      response.end(text);
    }
    if (request.url === '/getgql') {
      let text = await Reddit.getGQL();
      response.writeHead(200);
      response.end(text);
    }
    if (request.url === '/getsub') {
      let data = await parse(request, response) || '{}';
      try {
        data = JSON.parse(data);
      } catch(e) {
        return failed('A001');
      }
      // console.log({data});
      let text = await Reddit.getSub(data);
      response.writeHead(200);
      response.end(text);
    }
    if (request.url === '/fetchtitle') {
      let data = await parse(request, response) || '';
      let text = await Reddit.fetchTitle(data);
      response.writeHead(200);
      response.end(text);
    }
    if (request.url === '/dns') {
      let data = await parse(request, response) || '';
      let address;
      try {
        let dns = await lookup(data);
        address = dns.address;
      } catch(e) {
        return failed('A001');
      }
      response.writeHead(200);
      response.end(address);
    }
    if (request.url === '/pin') {
      let data = await parse(request, response) || '{}';
      try {
        data = JSON.parse(data);
      } catch(e) {
        return failed('A001');
      }
      let text = await Pinterest.pin(data);
      response.writeHead(200);
      response.end(text);
    }
    if (request.url === '/tweet') {
      let data = await parse(request, response);
      let text = await Twitter.tweet(data);
      response.writeHead(200);
      response.end(text);
    }
    return redirect();
  }
  const s = http.createServer(app);
  // s.on('request', app);
  s.on('error', () => {
    // eslint-disable-next-line no-process-exit
    return process.exit(1);
  });
  s.listen(8080);
  wsServer(s);
}