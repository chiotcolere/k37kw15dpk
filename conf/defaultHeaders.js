const defaultHeaders = {
  'Accept': '*/*',
  'Connection': 'keep-alive',
  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.34 Safari/537.36',
  'Accept-Language': 'en-US,en;q=0.9,zh;q=0.8,zh-CN;q=0.7,zh-TW;q=0.6,ja;q=0.5,ko;q=0.4,ru;q=0.3'
};

module.exports = defaultHeaders;