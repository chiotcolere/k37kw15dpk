const fs = require('fs');
const { promisify } = require('util');
const youtubedl = require('youtube-dl');
const outputMsg = require('./outputMsg');
const uploadToMEGA = require('./uploadToMEGA');
const sendToRobot = require('./sendToRobot');
const allowCORS = require('./allowCORS');
const queue = require('./getQueue');
const { version } = require('../package.json');

const getInfo = promisify(youtubedl.getInfo);

module.exports = async (target, response, failed, flags = []) => {
  try {
    const info = await getInfo(target);
    const {
      _filename,
      // format,
      duration,
      url,
    } = info;
    const filename = `[youtube-dl]${_filename}`;
    allowCORS(200, response);
    response.end(
      outputMsg({
        filename,
        duration,
        url,
        version,
        info: `\n${JSON.stringify(info, null, 2)}`,
      })
    );
    queue.add(
      () => new Promise((resolve, reject) => {
        youtubedl.exec(target, ['-o', filename, ...flags], {}, async e => {
          if (e) {
            sendToRobot(`youtube-dl failed\n${e.message}\n${e.stack}`);
            reject(e);
            return;
          }
          const stream = await (async () => {
            try {
              return fs.createReadStream(filename);
            } catch (e) {
              await new Promise(r => setTimeout(r, 1000));
              return fs.createReadStream(filename);
            }
          })();
          sendToRobot(`youtube-dl downloaded ${filename}`);
          try {
            uploadToMEGA( {
              filename,
              filesize: fs.statSync(filename).size,
              stream,
            });
          } catch (e) {
            sendToRobot(`youtube-dl no file downloaded ${e.message}`);
          }
          resolve({ filename });
        });
      })
    ).catch(() => {});
  } catch (e) {
    sendToRobot(`youtube-dl exec failed\n${e.message}\n${e.stack}`);
    failed(e.message);
  }
};