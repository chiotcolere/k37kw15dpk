const zlib = require('zlib');
const lzjb = require('lzjb');

let _type = 0;

const compress = data => {
  if (!Buffer.isBuffer(data)) {
    return;
  }
  const type = _type;
  _type = _type ? 0 : 1;
  return Buffer.concat([
    Buffer.from([type]),
    type
      ? zlib.gzipSync(data)
      : Buffer.from(lzjb.compressFile(data))
  ]);
}

module.exports = compress;