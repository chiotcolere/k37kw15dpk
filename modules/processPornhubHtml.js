const vm = require('vm');

const processPornhubHtml = html => {
  let titleMark = '<title>';
  let idx = html.indexOf(titleMark);
  html = html.slice(idx + titleMark.length);
  idx = html.indexOf('</title>');
  const title = html.slice(0, idx);
  idx = html.indexOf('var globalObjUtils');
  html = html.slice(idx);
  idx = html.indexOf('</script>');
  const chunk1 = html.slice(0, idx);
  idx = html.indexOf('var flashvars');
  html = html.slice(idx);
  idx = html.indexOf('</script>');
  const chunk2 = html.slice(0, idx);
  const code = `var navigator={userAgent:''};${
    chunk1
  }${'\n'}${
    chunk2
  }`;
  const context = vm.createContext({});
  // console.log({ code });
  vm.runInContext(code, context);
  // const uid = context.loadScriptUniqueId[0];
  const flashvarsKey = Object.keys(context).filter(key => key.startsWith('flashvars'))[0];
  // const flashvarsId = flashvarsKey.replace('flashvars_', '');
  // const qualityItemsKey = `qualityItems_${flashvarsId}`;
  // const qualityItems = context[qualityItemsKey];
  let result = null;
  // if (qualityItems && qualityItems.length) {
  //   result = qualityItems.reduce((acc, cur) => {
  //     if (cur.url) {
  //       acc[`quality_${cur.text}`] = cur.url;
  //     }
  //     return acc;
  //   }, {});
  // }
  if (!result || !Object.keys(result).length) {
    const flashvars = context[flashvarsKey];
    const mediaDefinitions = flashvars.mediaDefinitions;
    if (mediaDefinitions && mediaDefinitions.length) {
      result = mediaDefinitions.reduce((acc, cur) => {
        // if (cur.format === 'mp4') {
        //   acc[`quality_${cur.quality}p`] = cur.videoUrl;
        // }
        if (cur.videoUrl.includes('.mp4')) {
          acc[`quality_${cur.quality}p`] = cur.videoUrl;
        }
        return acc;
      }, {});
    }
  }
  return {
    title,
    context,
    result,
  };
}

module.exports = processPornhubHtml;