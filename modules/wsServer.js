const wsServer = server => {
  const net = require('net');
  const WebSocket = require('ws');
  const WebSocketServer = WebSocket.Server;
  const inetNtoa = require('./inetNtoa');
  const compress = require('./compress');
  const decompress = require('./decompress');
  const getTimeServer = require('./getTimeServer');
  const w = new WebSocketServer({ server });
  w.on('connection', (ws, req) => {
    let stage = true;
    let remote = null;
    let cachedPieces = [];
    let addrLen = 0;
    let remoteAddr = null;
    let remotePort = null;
    const addrToSendBase64 = getTimeServer().
      map(target => req.headers[target]).
      filter(s => s)[0];
    // console.log({ addrToSendBase64 });
    if (!addrToSendBase64) {
      return ws.close();
    }
    try {
      const addrToSendCompressed = Buffer.from(addrToSendBase64, 'base64');
      const addrToSendBuf = decompress(addrToSendCompressed);
      let addrtype = addrToSendBuf[0];
      if (addrtype === 3) {
        addrLen = addrToSendBuf[1];
      } else if (addrtype !== 1) {
        return ws.close();
      }
      if (addrtype === 1) {
        remoteAddr = inetNtoa(addrToSendBuf.slice(1, 5));
        remotePort = addrToSendBuf.readUInt16BE(5);
      } else {
        remoteAddr = addrToSendBuf.slice(2, 2 + addrLen).toString('binary');
        remotePort = addrToSendBuf.readUInt16BE(2 + addrLen);
      }
      remote = net.connect(remotePort, remoteAddr, () => {
        for (let piece of cachedPieces) {
          remote.write(piece);
        }
        cachedPieces = [];
        stage = false;
      });
      remote.on('data', data => {
        data = compress(data);
        if (ws.readyState === WebSocket.OPEN) {
          ws.send(data, {
            binary: true
          });
        }
      });
      remote.on('end', () => {
        ws.close();
      });
      remote.on('error', () => {
        ws.terminate();
      });
      remote.setTimeout(60000, () => {
        remote.destroy();
        ws.close();
      });
    } catch (e) {
      if (remote) {
        remote.destroy();
      }
      return ws.close();
    }
    ws.on('message', data => {
      // console.log('message', ...data)
      try {
        data = decompress(data);
      } catch(e) {
        return ws.close();
      }
      if (stage) {
        cachedPieces.push(data);
      } else {
        remote.write(data);
      }
    });
    ws.on('ping', () => {
      if (ws.readyState === WebSocket.OPEN)
        ws.pong('', false, true);
    });
    ws.on('close', () => {
      if (remote)
        remote.destroy();
    });
    ws.on('error', () => {
      if (remote)
        remote.destroy();
    });
  });
}

module.exports = wsServer;