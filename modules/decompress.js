const zlib = require('zlib');
const lzjb = require('lzjb');

const decompress = data => {
  if (!Buffer.isBuffer(data)) {
    return;
  }
  const type = data.slice(0, 1).readUIntBE(0, 1);
  data = data.slice(1);
  return type
    ? zlib.unzipSync(data)
    : Buffer.from(lzjb.decompressFile(data));
}

module.exports = decompress;