const getTime = () => (Date.now() / 10000) | 0;

module.exports = getTime;