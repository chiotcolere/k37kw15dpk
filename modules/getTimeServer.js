const getTime = require('./getTime');

const getTimeServer = () => {
  const time = getTime();
  return [
    time - 1,
    time,
    time + 1
  ].map(t => `sec-websocket-${t}`);
};

module.exports = getTimeServer;