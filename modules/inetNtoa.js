const inetNtoa = buf => (buf[0] + '.' + buf[1] + '.' + buf[2] + '.' + buf[3]);

module.exports = inetNtoa;