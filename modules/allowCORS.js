const allowCORS = (code, res) => {
  res.writeHead(code, {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'Content-Type, Content-Length, Accept, X-Requested-With',
    'Access-Control-Allow-Methods': 'POST'
  });
}

module.exports = allowCORS;