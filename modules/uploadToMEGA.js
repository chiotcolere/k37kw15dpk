const got = require('got');
const fs = require('fs');
const defaultHeaders = require('../conf/defaultHeaders');
const sendToRobot = require('./sendToRobot');
const { getStorage } = require('./getStorage');
const outputMsg = require('./outputMsg');
const rmFile = require('./rmFile');
const queue = require('./getQueue');

const { version } = require('../package.json');

const upload = async ({
  url,
  cookie,
  filename,
  filesize,
  stream,
  retried = 0,
}) => {
  const fetchFailed = (type, resolve, e) => {
    const errmsg = `uploadToMEGA ${url} ${filename} fetch on ${type}: ${e.message}`;
    sendToRobot(errmsg);
    resolve();
  }
  try {
    const megaStream = await getStorage();
    const getFetch = resolve => {
      const headers = cookie ? {
        ...defaultHeaders,
        cookie
      } : defaultHeaders;
      const theStream = got({
        url,
        headers,
        // gzip: true,
        decompress: true,
        // http2: true,
        isStream: true
      })
        .on('error', fetchFailed.bind(null, 'error', resolve))
        .on('timeout', fetchFailed.bind(null, 'timeout', resolve))
        .on('abort', fetchFailed.bind(null, 'abort', resolve))
        .on('response', res => {
          filesize = +res.headers['content-length'];
          sendToRobot(`uploadToMEGA ${url} ${filename} fetch on response:\n${
            outputMsg({
              ...res.headers,
              filename,
              url,
              version
            })
          }`);
          resolve(theStream);
        });
      return theStream;
    }
    let globalInfo;
    let fetchStream;
    if (stream) {
      fetchStream = stream;
    } else if (!filesize) {
      fetchStream = await new Promise(getFetch);
    } else {
      fetchStream = getFetch(() => {});
    }
    if (!fetchStream) {
      sendToRobot(`uploadToMEGA ${filename} no fetchStream and no filesize ${url}`);
      return;
    }
    return new Promise(resolve => {
      megaStream
        .upload({
          name: filename || `${Date.now()}`,
          size: isNaN(filesize) ? 0 : filesize
        }, fetchStream)
        .on('progress', info => {
          globalInfo = info;
        })
        .on('complete', () => {
          globalInfo = globalInfo || {};
          globalInfo.completed = true;
          sendToRobot(`uploadToMEGA completed: ${filename}`);
          if (filename.startsWith('[youtube-dl]')) {
            rmFile(filename);
          }
          resolve(globalInfo);
        })
        .on('error', e => {
          sendToRobot(`uploadToMEGA ${url || 'stream'} ${filename} on error: ${e.message}`);
          if (filename.startsWith('[youtube-dl]')) {
            if (retried < 3) {
              queue.add(
                uploadToMEGA({
                  cookie,
                  filename,
                  filesize,
                  stream: fs.createReadStream(filename),
                  retried: retried + 1
                })
              ).catch(() => {});
            } else {
              rmFile(filename);
              rmFile(`${filename}.part`, true);
            }
          }
          resolve(globalInfo);
        });
      const progressReporter = () => setTimeout(() => {
        if (!globalInfo) {
          sendToRobot(`uploadToMEGA progress: ${filename}\nNo progress event has been emitted`);
          return;
        }
        const {
          bytesLoaded,
          bytesTotal,
          lastPercentage,
          completed
        } = globalInfo;
        if (completed) return;
        const percentage = parseInt((bytesLoaded / bytesTotal) * 100, 10);
        if (lastPercentage === percentage) {
          sendToRobot(`uploadToMEGA progress: ${filename}\nUploader probably died ${percentage}%`);
          return;
        }
        globalInfo.lastPercentage = percentage;
        sendToRobot(`uploadToMEGA progress: ${filename}\n${bytesLoaded} bytesLoaded\n${bytesTotal} bytesTotal\n${percentage}% percentage`);
        if (percentage < 90) {
          progressReporter();
        }
      }, 60000);
      progressReporter();
    });
  } catch (e) {
    sendToRobot(`uploadToMEGA ${url} ${filename} catch error: ${e.message}\n${e.stack}`);
  }
}

const uploadToMEGA = async ({
  url,
  cookie,
  filename,
  filesize,
  stream,
  retried = 0,
}) => {
  queue.add(
    upload({
      url,
      cookie,
      filename,
      filesize,
      stream,
      retried,
    })
  ).catch(() => {});
};

module.exports = uploadToMEGA;