const fs = require('fs');
const sendToRobot = require('./sendToRobot');

const rmFile = (filename, silent) => {
  try {
    fs.rmSync(filename);
  } catch (e) {
    !silent && sendToRobot(`rmSync failed ${e.message}`);
  }
}

module.exports = rmFile;