const fs = require('fs');
const uploadToMEGA = require('./uploadToMEGA');
const sendToRobot = require('./sendToRobot');

module.exports = () => {
  try {
    const videos = fs.readdirSync('.').filter(name => name.endsWith('.mp4'));
    videos.forEach(filename => {
      try {
        uploadToMEGA({
          response:{
            writeHead: () => {},
            end: () => {},
          },
          filename,
          filesize: fs.statSync(filename).size,
          failed: () => {},
          stream: fs.createReadStream(filename),
        });
      } catch (e) {
        sendToRobot(`findVideosToUpload no file found ${filename} ${e.message}`);
      }
    })
  } catch (e) {}
};