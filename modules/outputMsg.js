const outputMsg = data => Object.entries(data).map(arr => arr.join(': ')).join('\n');

module.exports = outputMsg;