/* eslint-disable no-console */
const got = require('got');
const reg = /(.{1024})/s;

const sendToRobot = async text => {
  console.log('sendToRobot msg', text);
  if (typeof text !== 'string') return;
  const array = text.split(reg).filter(Boolean);
  for (let content of array) {
    await got({
      url: 'https://aero-sg.us.to/wework/sendWeworkMsg',
      method: 'POST',
      headers: {
        a: 'covid-mp',
      },
      json: {
        content,
        secret: '2B404F59A6F7D97E8C9FDFC9515FC290',
      },
    }).catch(e => {
      console.error('sendToRobot failed', e.message);
    });
  }
};

module.exports = sendToRobot;