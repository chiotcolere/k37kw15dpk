const mega = require('megajs-2');

const storageOpts = {
  email: 'aero.windwalker@gmail.com',
  password: 'K2816$dxdb0',
  autologin: false
};

const storage = new Map();

const getStorage = async () => {
  if (!storage.get('useable')) {
    const target = new mega.Storage(storageOpts);
    const promise = new Promise(resolve => {
      target.login((e, stream) => {
        if (e) {
          storage.set('useable', false);
        } else {
          storage.set('useable', true);
        }
        resolve(stream);
      });
    });
    storage.set('promise', promise);
    return promise;
  }
  return storage.get('promise');
}

module.exports = {
  getStorage,
};