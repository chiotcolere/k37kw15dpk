const request = require('request-promise-native');
// const crypto = require('crypto');

class Instagram {
  constructor({
    csrftoken = '',
    sessionid = '',
    // rhx_gis = '',
    rollout_hash = '',
    www_claim = '0'
  } = {}) {
    this.csrftoken = csrftoken;
    this.sessionid = sessionid;
    // this.rhx_gis = rhx_gis;
    this.www_claim = www_claim;
    this.rollout_hash = rollout_hash;
    this.userAgent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36';
    this.jar = {
      csrftoken,
      sessionid
    }
  }
  getFeed(count = 12, cursor = '', query_hash = '') {
    let variables = JSON.stringify({
      fetch_media_item_count: count,
      fetch_media_item_cursor: cursor
    });
    // let headers = {
    //   ...this.getHeaders(),
    //   'x-instagram-gis': crypto.createHash('md5').update(`${this.rhx_gis}:${variables}`, 'utf8').digest('hex')
    // }
    let options = {
      url: `https://www.instagram.com/graphql/query/?query_hash=${query_hash}&variables=${encodeURIComponent(variables)}`,
      headers: this.getHeaders(),
      transform(body, response) {
        let { headers } = response;
        return { headers, body}
      }
    };
    // console.log(options);
    return request(options).then(res => {
      this.setCookie(res.headers['set-cookie']);
      return res.body;
    });
  }
  getUserFeed({id, first = 12, after = '', query_hash = ''} = {}){
    let variables = JSON.stringify({
      id,
      first,
      after
    });
    // let headers = {
    //   ...this.getHeaders(),
    //   'x-instagram-gis': crypto.createHash('md5').update(`${this.rhx_gis}:${variables}`, 'utf8').digest('hex')
    // };
    let options = {
      method: 'POST',
      url: `https://www.instagram.com/graphql/query/?query_hash=${query_hash}&variables=${encodeURIComponent(variables)}`,
      headers: this.getHeaders(),
      transform(body, response) {
        let { headers } = response;
        return { headers, body};
      }
    };
    return request(options).then(res => {
      this.setCookie(res.headers['set-cookie']);
      return res.body;
    });
  }
  user(username){
    if (!username) {
      return;
    }
    // let headers = {
    //   ...this.getHeaders(),
    //   'x-instagram-gis': crypto.createHash('md5').update(`${this.rhx_gis}:${username}`, 'utf8').digest('hex')
    // };
    let options = {
      method: 'GET',
      url: `https://www.instagram.com/${username}/?__a=1`,
      headers: this.getHeaders(),
      transform(body, response) {
        let { headers } = response;
        return { headers, body}
      }
    };
    return request(options).then(res => {
      this.setCookie(res.headers['set-cookie']);
      return res.body;
    });
  }
  userWeb(username){
    if (!username) {
      return;
    }
    let headers = {
      // 'authority': 'www.instagram.com',
      'origin': 'https://www.instagram.com',
      'cookie': this.getCookie(),
      'pragma': 'no-cache',
      'user-agent': this.userAgent
    };
    let options = {
      method: 'GET',
      url: `https://www.instagram.com/${username}/`,
      headers,
      transform(body, response) {
        let { headers } = response;
        return { headers, body}
      }
    };
    return request(options).then(res => {
      this.setCookie(res.headers['set-cookie']);
      const anchor = 'window._sharedData = ';
      let response = res.body.slice(res.body.indexOf(anchor) + anchor.length);
      response = response.slice(0, response.indexOf(';</script>'));
      return response;
    });
  }
  follow(url, id){
    let headers = {
      ...this.getHeaders(),
      'x-instagram-ajax': this.rollout_hash
    };
    let options = {
      method: 'POST',
      url: `https://www.instagram.com/web/friendships/${id}${url}/`,
      headers,
      transform(body, response) {
        let { headers } = response;
        return { headers, body}
      }
    };
    return request(options).then(res => {
      this.setCookie(res.headers['set-cookie']);
      return res.body;
    });
  }
  like(url, id){
    let headers = {
      ...this.getHeaders(),
      'x-instagram-ajax': this.rollout_hash
    };
    let options = {
      method: 'POST',
      url: `https://www.instagram.com/web/likes/${id}${url}/`,
      headers,
      transform(body, response) {
        let { headers } = response;
        return { headers, body}
      }
    };
    return request(options).then(res => {
      this.setCookie(res.headers['set-cookie']);
      return res.body;
    });
  }
  getHeaders() {
    let cookie = this.getCookie();
    return {
      // 'authority': 'www.instagram.com',
      'sec-fetch-mode': 'cors',
      'origin': 'https://www.instagram.com',
      'referer': 'https://www.instagram.com',
      'user-agent': this.userAgent,
      'x-requested-with': 'XMLHttpRequest',
      'x-csrftoken': this.csrftoken,
      'x-ig-app-id': '936619743392459',
      'x-ig-www-claim': this.www_claim,
      'pragma': 'no-cache',
      'accept': '*/*',
      cookie
    };
  }
  getCookie() {
    let array = [];
    for (let key in this.jar) {
      array.push(`${key}=${this.jar[key]}`)
    }
    return array.join(';');
  }
  setCookie(cookies) {
    if (!Array.isArray(cookies)) return;
    for (let cookie of cookies) {
      cookie = ( cookie.split(';')[0] ).split('=');
      let key = cookie[0], value = cookie[1];
      if (value) {
        this.jar[key] = value;
      }
    }
    return this.jar;
  }
  config({
    csrftoken,
    sessionid,
    // rhx_gis = '',
    www_claim,
    rollout_hash
  } = {}) {
    if (csrftoken) {
      this.jar.csrftoken = this.csrftoken = csrftoken;
    }
    if (sessionid) {
      this.jar.sessionid = this.sessionid = sessionid;
    }
    if (www_claim) {
      this.www_claim = www_claim;
    }
    if (rollout_hash) {
      this.rollout_hash = rollout_hash;
    }
  }
}

module.exports = Instagram