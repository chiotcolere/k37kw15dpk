FROM node:12-alpine

ADD conf /app/conf
ADD modules /app/modules
ADD package.json /app
ADD *.js /app

RUN cd /app; npm install

ENV NODE_ENV production
ENV PORT 8080
EXPOSE 8080

WORKDIR "/app"
CMD [ "npm", "start" ]